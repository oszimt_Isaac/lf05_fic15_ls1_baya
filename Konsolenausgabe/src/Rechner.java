import java.util.Scanner; //Importanweisung für den Scanner

public class Rechner 
{
  
  public static void main(String[] args) 
  {
    
    int zahl1 = 0, zahl2 = 0, addition , division, subtraktion, multiplikation; // Deklaration
    
    Scanner sc = new Scanner(System.in); // Neues Scanner-Objekt wird erstellt     
    
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");   
    zahl1 = sc.nextInt(); // Variable speichert die erste Eingabe
    
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
    zahl2 = sc.nextInt(); // Variable speichert die zweite Eingabe
    
    addition = zahl1 + zahl2;
    subtraktion = zahl1 - zahl2;
    division = zahl1 / zahl2;
    multiplikation = zahl1 * zahl2;
    
    
    System.out.print("\n\n\nErgebnis der Addition lautet: ");
    System.out.print(zahl1 + " + " + zahl2 + " = " + addition );
    
    System.out.print("\nErgebnis der Subtraktion lautet: ");
    System.out.print(zahl1 + " - " + zahl2 + " = " + subtraktion);      
    
    System.out.print("\nErgebnis der Division lautet: ");
    System.out.print(zahl1 + " / " + zahl2 + " = " + division );
    
    System.out.print("\nErgebnis der Multiplikation lautet: ");
    System.out.print(zahl1 + " * " + zahl2 + " = " + multiplikation );        
    
    sc.close();

  }   
}